'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', ({
  response
}) => {
  return response.status('200').json({
    status: 200,
    message: 'RestFull Api'
  })
})

Route.group(() => {
  Route.post('login', 'UserController.login')
  Route.get('logout', 'UserController.logout').middleware(['auth:jwt'])
  Route.post('register', 'UserController.register')
}).prefix('api')

Route.group(() => {
  Route.get('print', 'StudentController.generatePdf')
  Route.get('export', 'StudentController.export')
  Route.post('import', 'StudentController.import')
  Route.resource('students', 'StudentController').validator(new Map([
    [['students.store'], ['StoreStudent']]
  ]))
}).prefix('api/v1').middleware(['auth:jwt'])

Route.get('export', 'StudentController.export')

