'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class StudentSchema extends Schema {
  up () {
    this.table('students', (table) => {
      table.integer('user_id').unsigned()
    })
  }

  down () {
    this.table('students', (table) => {
      table.integer('user_id').unsigned()
    })
  }
}

module.exports = StudentSchema
