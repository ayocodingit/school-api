FROM node:10.23-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install

RUN npm i -g @adonisjs/cli

COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]
