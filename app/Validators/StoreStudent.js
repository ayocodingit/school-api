'use strict'

class StoreStudent {
  get validateAll() {
    return true
  }

  get rules() {
    return {
      nisn: 'required',
      name: 'required',
      study: 'required'
    }
  }

  get messages() {
    return {
      'nisn.required': 'You must provide a email address.',
      'name.required': 'You must provide a valid email address.',
      'study.required': 'This email is already registered.',
    }
  }

  async fails(errorMessages) {
    return this.ctx.response.json({
      status: 422,
      result: errorMessages
    })
  }
}

module.exports = StoreStudent
