'use strict'

const Excel = require('exceljs')
const Student = use('App/Models/Student')
const fs = require('fs')
const {
  validateAll
} = use('Validator')

class ImportService {
  static async ImportStudent(filelocation) {
    let workbook = new Excel.Workbook()
    workbook = await workbook.xlsx.readFile(filelocation)
    const explanation = workbook.getWorksheet('Student')
    const colComment = explanation.getColumn('A')
    colComment.eachCell(async (cell, rowNumber) => {
      if (rowNumber >= 2) {
        const nisn = explanation.getCell('A' + rowNumber).value
        const name = explanation.getCell('B' + rowNumber).value
        const study = explanation.getCell('C' + rowNumber).value
        const rules = {
          nisn: 'required',
          name: 'required',
          study: 'required'
        }
        const data = {
          nisn: nisn,
          name: name,
          study: study
        }
        const validation = await validateAll(data, rules)
        if (validation.fails()) {
          return validation.messages()
        } else {
          const student = new Student
          student.nisn = nisn
          student.name = name
          student.study = study
          await student.save()
        }
      }
    })
    await fs.unlinkSync(filelocation)
  }

  static async ExportStudent() {
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('Student');
    worksheet.columns = [{
        header: 'No',
        key: 'id',
        width: 5
      },
      {
        header: 'NISN',
        key: 'nisn',
        width: 10
      },
      {
        header: 'Name',
        key: 'name',
        width: 10
      },
      {
        header: 'Study',
        key: 'study',
        width: 10
      }
    ];
    worksheet.columns.forEach(column => {
      column.border = {
        top: {
          style: "medium"
        },
        left: {
          style: "medium"
        },
        bottom: {
          style: "medium"
        },
        right: {
          style: "medium"
        }
      };
    });
    const models = await Student.all()
    models.toJSON().forEach(item => {
      let data = [
        item.id,
        item.nisn,
        item.name,
        item.study
      ]
      worksheet.addRow(data);
    })
    const fname = 'Student.xlsx'
    const path = 'tmp/upload/' + fname;

    if (!fs.existsSync('tmp/upload')) {
      await fs.mkdirSync('tmp/upload');
    }

    if (fs.existsSync(path)) {
      await fs.unlinkSync(path)
    }

    await workbook
      .xlsx
      .writeFile(path)
      .then(() => {
        console.log("saved");
      })
      .catch((err) => {
        console.log("err", err);
      });
    return fname
  }

}

module.exports = ImportService
