'use strict'

const User = use('App/Models/User')

class UserController {
  async login({
    request,
    auth,
    response
  }) {
    const {
      email,
      password
    } = request.all()
    try {
      const jwt = await auth.authenticator('jwt').withRefreshToken().attempt(email, password)
      return response.status(200).json({
        status: 200,
        message: 'success',
        result: jwt
      })
    } catch (error) {
      return response.status(500).json({
        status: 500,
        message: error.message
      })
    }
  }

  async logout({
    auth,
    response
  }) {
    const apiToken = await auth.getAuthHeader()
    await auth.authenticator('jwt').revokeTokens([apiToken], true)
    return response.status(200).json({
      status: 200,
      message: 'Logout !',
    })
  }

  async register({
    response,
    request
  }) {
    const {
      username,
      email,
      password
    } = request.all()
    const user = new User()
    user.username = username
    user.email = email
    user.password = password
    await user.save()
    return response.status(200).json(user)
  }
}

module.exports = UserController
