'use strict'

const Student = use("App/Models/Student")
const Database = use('Database')
const PDF = use('PDF')
const SpreadSheet = use('SpreadSheet')
const Helpers = use('Helpers')
const ImportService = use('App/Services/ImportService')
const Excel = require('exceljs')
const fs = require('fs')
const {
  validate
} = use('Validator')

class StudentController {
  async store({
    request,
    response
  }) {
    const DB = await Database.beginTransaction()
    try {
      const studentInfo = request.only(['nisn', 'name', 'study'])
      const student = new Student()
      student.nisn = studentInfo.nisn
      student.name = studentInfo.name
      student.study = studentInfo.study
      await student.save(DB)
      await DB.commit()
      return response.status(200).json({
        status: 200,
        message: 'success',
        result: student
      })
    } catch (error) {
      await DB.rollback()
      return response.status(500).json({
        status: 500,
        message: 'terjadi kesalahan dengan server'
      })
    }
  }

  async index({
    request,
    response
  }) {
    let models = Student.query().with(['user'])

    let search = request.input('search', false)
    let params = request.input('params', false)
    let order = request.input('order', 'nisn')
    let page = request.input('page', 1)
    let perpage = request.input('perpage', 20)

    if (params) {
      params = JSON.parse(params)
      for (const key in params) {
        if (!params[key] || params[key] == '') {
          continue
        }
        switch (key) {
          case "nisn":
          case "name":
          case "study":
            models = models.where(key, 'ilike', '%' + params[key] + '%')
            break;
        }
      }
    }

    if (search) {
      models = models.where('nisn', 'ilike', '%' + search + '%')
        .orWhere('name', 'ilike', '%' + search + '%')
        .orWhere('study', 'ilike', '%' + search + '%')
        .orWhereHas('user', query => {
          query.where('email', 'ilike', '%' + search + '%')
        })
    }

    if (order) {
      let order_direction = request.input('order_direction', 'asc')
      switch (order) {
        case "nisn":
        case "name":
        case "study":
          models = models.orderBy(order, order_direction)
          break;
      }
    }

    models = await models.paginate(page, perpage)
    return response.status(200).json({
      status: 200,
      message: 'success',
      result: models,
    })
  }

  async show({
    params,
    response
  }) {
    const student = await Student.query().with(['user']).where('id', params.id).first()
    if (!student) {
      return response.status(404).json({
        status: 404,
        message: 'Resource not found'
      })
    }

    return response.status(200).json({
      status: 200,
      message: 'success',
      result: student
    })
  }

  async update({
    request,
    params,
    response
  }) {
    const DB = await Database.beginTransaction()
    try {
      const studentInfo = request.only(['nisn', 'name', 'study'])
      const student = await Student.find(params.id)
      student.nisn = studentInfo.nisn
      student.name = studentInfo.name
      student.study = studentInfo.study
      await student.save(DB)
      await DB.commit()
      return response.status(200).json({
        status: 200,
        message: 'success',
        result: student
      })
    } catch (error) {
      DB.rollback()
      return response.status(500).json({
        status: 500,
        message: 'terjadi kesalahan dengan server'
      })
    }
  }

  async destroy({
    params,
    response
  }) {
    const DB = await Database.beginTransaction()
    try {
      const student = await Student.find(params.id)
      await student.delete(DB)
      await DB.commit()
      return response.status(200).json({
        status: 200,
        message: 'success',
        result: []
      })
    } catch (error) {
      DB.rollback()
      return response.status(500).json({
        status: 500,
        message: 'terjadi kesalahan dengan server'
      })
    }
  }

  async generatePdf({
    response
  }) {

    const content = [{
      text: 'lorem sum'
    }]

    PDF.create(content, response.response)

    return response
  }

  async export ({
    response
  }) {
    const fname = await ImportService.ExportStudent()
    response.header('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response.header("Content-disposition", "attachment; filename=" + fname)
    return response.download(Helpers.tmpPath('/upload/' + fname))
  }

  // async export({
  //   response
  // }) {
  //   const ss = new SpreadSheet(response, 'xlsx')

  //   const users = await Student.all()
  //   const data = []

  //   data.push([
  //     'id',
  //     'Nisn',
  //     'Name',
  //     'Study',
  //   ])

  //   users.toJSON().forEach(user => {
  //     data.push([
  //       user.id,
  //       user.nisn,
  //       user.name,
  //       user.study
  //     ])
  //   })

  //   ss.addSheet('Student', data)
  //   ss.download('student-export')
  // }

  async import({
    request,
    response
  }) {
    let upload = request.file('upload')
    let fname = `${new Date().getTime()}.${upload.extname}`
    let dir = 'upload/'

    await upload.move(Helpers.tmpPath(dir), {
      name: fname
    })

    if (!upload.moved()) {
      return response.status(500).json({
        status: 500,
        message: 'Error moving files'
      })
    }

    const result = await ImportService.ImportStudent('tmp/' + dir + fname)
    console.log(result);
    return response.status(200).json({
      status: 200,
      message: 'Import File Success!',
    })
  }

  async ImportStudent(filelocation) {
    let workbook = new Excel.Workbook()
    workbook = await workbook.xlsx.readFile(filelocation)
    const explanation = workbook.getWorksheet('Student')
    const colComment = explanation.getColumn('A')
    colComment.eachCell(async (cell, rowNumber) => {
      if (rowNumber >= 2) {
        const nisn = explanation.getCell('A' + rowNumber).value
        const name = explanation.getCell('B' + rowNumber).value
        const study = explanation.getCell('C' + rowNumber).value
        const rules = {
          nisn: 'required',
          name: 'required',
          study: 'required'
        }
        const data = {
          nisn: nisn,
          name: name,
          study: study
        }
        const validation = await validate(data, rules)
        if (validation.fails()) {
          return validation.messages()
        }else{
          const student = new Student
          student.nisn = nisn
          student.name = name
          student.study = study
          await student.save()
        }
      }
    })
    await fs.unlinkSync(filelocation)
  }
}

module.exports = StudentController
